#include "MainWin.h"
#include "QMessageBox"
#include <QDebug>

void MainWin::initMember()
{
    m_client.setHandler(this);
}


void MainWin::sendBtnClicked()
{

}

void MainWin::logInOutBtnClicked()
{
    int a = loginDlg.exec();
    if(  a == QDialog::Accepted )
    {
        QString usr = loginDlg.getUser().trimmed();
        QString pwd = loginDlg.getPwd();

        if( m_client.connectTo("127.0.0.1", 8890))
        {
            TextMessage tm1("tm1","来自client1的消息！");
            TextMessage tm2("tm2","来自client2的消息！");
            TextMessage tm3("tm3","来自client3的消息！");
            m_client.send(tm1);
            m_client.send(tm2);
            m_client.send(tm3);
            //setCtrlEnabled(true);
        }
        else
        {
            QMessageBox::critical(this,"失败","无法连接远程服务器");
        }
    }
}

void MainWin::handle(QTcpSocket& obj, TextMessage& message)
{
    qDebug() << message.type();
    qDebug() << message.data();
}

