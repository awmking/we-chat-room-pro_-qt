#include "testmessage.h"

TextMessage::TextMessage(QObject* parent) : QObject(parent)
{
    m_type = "";
    m_data = "";
}

TextMessage::TextMessage(QString type, QString data, QObject* parent) : QObject(parent)
{
    m_type = type.trimmed();

    m_type.resize(4, ' ');//强制使用4个字符，协议定义头部type使用4个字节，不足4个字符则用空格填充

    //m_data = data.mid(0, 0xFFFF);//取子串两个字节，也就是每个字节的消息最多传递两个字节的数据
    m_data = data.mid(0, 15000);
}

QString TextMessage::type()
{
    return m_type.trimmed();
}

int TextMessage::length()
{
    return m_data.length();
}

QString TextMessage::data()
{
    return m_data;
}

// 协议组装
QByteArray TextMessage::serialize()
{
#if 0
    QString len = QString::asprintf("%X", m_data.length());//把数据区的长度格式化为16进制表示的字符串
    len.resize(4, ' ');//把长度也转换为4个字符
    return m_type + len + m_data;//组装成协议规定的格式
#endif
    QByteArray ret;
    QByteArray dba = m_data.toUtf8();
    QString len = QString::asprintf("%X", dba.length());

    len.resize(4, ' ');

    ret.append(m_type.toStdString().c_str(), 4);
    ret.append(len.toStdString().c_str(), 4);
    ret.append(dba);

    return ret;
}

//把符合协议定义的字符串，转换为合法的协议对象
bool TextMessage::unserialize(QByteArray ba)
{
    bool ret = (ba.length() >= 8);

    if( ret )
    {
        QString type = QString(ba.mid(0, 4));
        QString len = QString(ba.mid(4, 4)).trimmed();
        int l = len.toInt(&ret, 16);

        ret = ret && (l == (ba.length() - 8));

        if( ret )
        {
            m_type = type;
            m_data = QString(ba.mid(8)); // QString完成了解码操作
        }
    }

    return ret;
}
