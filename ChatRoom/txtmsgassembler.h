#ifndef TXTMSGASSEMBLER_H
#define TXTMSGASSEMBLER_H

#include <QObject>
#include <QQueue>
#include <QSharedPointer>
#include "testmessage.h"

class TxtMsgAssembler : public QObject
{
    QQueue<char> m_queue;//我的大仓库
    QString m_type;
    int m_length;
    QByteArray m_data;

    void clear();
    QByteArray fetch(int n);
    bool makeTypeAndLength();
    TextMessage* makeMessage();
public:
    TxtMsgAssembler(QObject* parent = NULL);
    void prepare(const char* data, int len);//缓冲区拿到数据转存到对象的仓库中m_queue
    QSharedPointer<TextMessage> assemble(const char* data, int len);
    QSharedPointer<TextMessage> assemble();//状态函数，装配对象的函数
    void reset();//如果状态函数出错，这个函数可以重置状态函数
};

#endif // TXTMSGASSEMBLER_H
