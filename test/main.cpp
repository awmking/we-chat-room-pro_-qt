#include <QCoreApplication>
#include <QTcpSocket>
#include <QDebug>
#include <QThread>
#include "ClientDemo.h"
#include "ServerDemo.h"
#include "testmessage.h"
#include "txtmsgassembler.h"
#include "txtmsghandler.h"


class Handler : public TxtMsgHandler
{
public:
    void handle(QTcpSocket& socket, TextMessage& message)
    {
        qDebug() << &socket;
        qDebug() << message.type();
        qDebug() << message.length();
        qDebug() << message.data();
    }
};


// tcp客户端的同步编程
void SyncClientDemo()
{
    QTcpSocket client;
    char buf[256] = {0};

    client.connectToHost("127.0.0.1", 8080);
    qDebug() << "Connected:" << client.waitForConnected();

    qDebug() << "Send Bytes:" << client.write("D.T.Software");
    qDebug() << "Send Status:" << client.waitForBytesWritten();

    qDebug() << "Data Available:" << client.waitForReadyRead();
    qDebug() << "Received Bytes:" << client.read(buf, sizeof(buf)-1);

    qDebug() << "Received Data:" << buf;

    client.close();
    //client.waitForDisconnected();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    /****************同步编程测试 ****************/
#if 0
    char data[] = "i will come here!";
    SyncClientDemo();
#endif
    /****************异步编程客户端测试 ****************/
#if 0
    QThread::sleep(3000);
    ClientDemo client;
    client.connectTo("127.0.0.1",8080);
    client.send(data,sizeof(data) - 1);
#endif

    /****************异步编程服务端测试 ****************/
#if 0
    //ServerDemo server;
    //server.start(8080);
#endif

    /****************文本协议的设计和实现测试 ****************/
#if 0
    TextMessage tm("AB", "1234567890");
    QString s = tm.serialize();

    qDebug() << s;

    TextMessage tmt;
    qDebug() << tmt.unserialize(s);
    qDebug() << tmt.type();
    qDebug() << tmt.length();
    qDebug() << tmt.data();
#endif
     /****************文本协议的设计和实现测试 ****************/
#if 0
    TextMessage tm("AB", "1234567890");
    QString s = tm.serialize();

    qDebug() << s;

    TxtMsgAssembler as;
    QSharedPointer<TextMessage> pt;  //使用智能指针 自动管理内存

    pt = as.assemble(s.toStdString().c_str(), s.length());

    if( pt != NULL )
    {
        qDebug() << "assemble successfully";
        qDebug() << pt->type();
        qDebug() << pt->length();
        qDebug() << pt->data();
    }

#endif

    /****************验证是否支持中文字符 ****************/
#if 0
   TextMessage tm("AB", "chinese go. 我是中文字符");
   QByteArray s = tm.serialize();

   qDebug() << s;

   qDebug() << "方式一："<< endl;
   TextMessage tmt;
   qDebug() << tmt.unserialize(s);
   qDebug() << tmt.type();
   qDebug() << tmt.length();
   qDebug() << tmt.data();


   qDebug() << "方式二："<< endl;
   TxtMsgAssembler as;
   QSharedPointer<TextMessage> pt;

   pt = as.assemble(s.data(), s.length());

   if( pt != NULL )
   {
       qDebug() << "assemble successfully";
       qDebug() << pt->type();
       qDebug() << pt->length();
       qDebug() << pt->data();
   }
//结论：在字符流的处理中，不支持中文字符的处理，因为在聊天室中，使用的是中文，因此这是一个很重要的问题
//分析：协议实现只考虑ASCII码的情况，对中文类型宽字符编码未考虑（宽字符 >= 2 字节）
#endif



    /****************文本协议的网络应用 ****************/
#if 1
   TextMessage message("Demo", "hei man. 你看起来很酷");
   Handler handler;
   ServerDemo server;
   ClientDemo client;

   server.setHandler(&handler);
   server.start(8890);

   client.setHandler(&handler);
   client.connectTo("127.0.0.1", 8890);
   client.send(message);

#endif
    return a.exec();
}
