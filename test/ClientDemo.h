#ifndef CLIENTDEMO_H
#define CLIENTDEMO_H

#include <QObject>
#include <QTcpSocket>
#include "TestMessage.h"
#include "TxtMsgAssembler.h"
#include "TxtMsgHandler.h"

class ClientDemo : public QObject
{
    Q_OBJECT

    QTcpSocket m_client;
    TxtMsgAssembler m_assembler;
    TxtMsgHandler* m_handler;

protected slots:
    void onConnected();
    void onDisconnected();
    void onDataReady();
    void onBytesWritten(qint64 bytes);

public:
    ClientDemo(QObject* parent = NULL);
    bool connectTo(QString ip, int port);
    qint64 send(TextMessage& message);
    qint64 available();
    void setHandler(TxtMsgHandler* handler);
    void close();
};

#endif // CLIENTDEMO_H

/*
tips:
Q_OBJECT 宏在 Qt 框架中的 C++ 类中非常关键，特别是当类需要使用 Qt 的元对象系统时，比如信号和槽的机制。这里是它的一些主要作用：
1. 信号和槽：使类能够定义信号和槽，这是 Qt 特有的一种通信机制。信号和槽用于对象之间的通信，信号在一个对象中被触发，并且可以连接到另一个对象的槽，当信号触发时，槽函数就会被调用。
2. 运行时类型信息：Q_OBJECT 宏允许类通过 Qt 的元对象系统提供运行时类型信息。这包括类名、父类名和属性等。
3. 动态属性系统：使得可以在运行时向对象添加额外的属性。
4. 元对象编译器（moc）：Q_OBJECT 宏是 Qt 元对象编译器（moc）的指示符，告诉 moc 这个类需要进行额外的处理以生成元代码。moc 生成的代码包含了实现信号和槽机制、动态属性等所需的辅助代码。
因此，在使用 Qt 框架并需要上述功能时，类定义中包含 Q_OBJECT 宏是必须的。如果去掉这个宏，类将不能使用信号和槽，也无法利用 Qt 的其他高级特性。
*/
